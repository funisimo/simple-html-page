# Simple HTML page

Page was particularly made as a developer exercise to show front-end development skills.

Project was completed within an hour.

Notes:

- Pictures could be better quality if provided
- Solutions on smaller screens were made without `best practises`, which usually are agreed across project within development teams
- To get started quicker for project was used a built in template of PHPstorm application
- No frameworks were used in this project.
- Few questions arise when developing page for designers and functional clarifications could be added
- As not required links all lead to google.com
- No Javascript were used as no elements on page require it
- Developer would prefer building this project using React framework